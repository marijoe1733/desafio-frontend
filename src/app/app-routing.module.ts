import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { EpisodesComponent } from './episodes/episodes.component';
import { CharactersListComponentComponent } from './characters-list-component/characters-list-component.component';
import { NotfoundComponent } from './notfound/notfound.component';


const routes: Routes = [
  { path: '', component: CharactersListComponentComponent },
  { path: 'character', component: CharactersListComponentComponent },
  { path: 'episodes', component: EpisodesComponent },
  { path: '404', component: NotfoundComponent},
  { path: '**', redirectTo: '/404'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
