import { Component, OnInit, NgModule, ViewChild} from '@angular/core';
import { CharacterService} from "../character.service";
import {ChangeDetectorRef } from '@angular/core';
import { MatExpansionPanel,MatAccordion } from '@angular/material/expansion';




@Component({
  selector: 'app-characters-list-component',
  templateUrl: './characters-list-component.component.html',
  styleUrls: ['./characters-list-component.component.scss']
})
export class CharactersListComponentComponent implements OnInit {
  

  @ViewChild('panel1') firstPanel: MatExpansionPanel;
    @ViewChild('accordion',{static:true}) Accordion: MatAccordion


  

  constructor(public _listado: CharacterService, private cdref: ChangeDetectorRef) { }
  list = [];

  ngOnInit(): void {
    this.getData();
    this.cdref.detectChanges();

  }

  getRandomColor() {
    var color = Math.floor(0x1000000 * Math.random()).toString(16);
    return '#' + ('000000' + color).slice(-6);
  }

  getData() {
    this._listado.getcharacters().subscribe(
        (res: any) => {
           
            this.list = res;
            console.log(this.list);
        
        },
        (error) => {
            console.log(error);
        }
    );
}
}
