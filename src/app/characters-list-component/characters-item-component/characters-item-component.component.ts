import { Component, OnInit, Input, ViewChild , Inject} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { CharacterService} from "../../character.service";
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { DOCUMENT } from '@angular/common';



@Component({
  selector: 'app-characters-item-component',
  templateUrl: './characters-item-component.component.html',
  styleUrls: ['./characters-item-component.component.scss']
})
export class CharactersItemComponentComponent implements OnInit {

  @Input() list : any[];
  @Input() index: number;
  @Input() firstPanel;
  @Input() accordion;

  formdata;
  successMsg = null;
  title = "Edit Character";
  
  constructor(public characterServive: CharacterService,
    private flashMessage: FlashMessagesService ,
    @Inject(DOCUMENT) private _document: Document
    ) { 
    
   }

  ngOnInit(): void {
    
   
  }

  onSubmit(form: NgForm) {
    const character = form.value;
    //console.log(character);
    this.characterServive.updatecharacter(character).subscribe(
      (res: any) => {
         
        this.successMsg = res;
        Swal.fire({
              
          text: 'Character updated successfully!',
          icon: 'success',
          confirmButtonText: 'Ok'
      }).then(()=> {

        this.closeAccordion();
        this.ngOnInit();
      })
        this.flashMessage.show('Updated Successfully', { cssClass: 'custom-success', timeout: 1000 });
      },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
        }).then(()=> {

          this.refreshPage();
        })
          console.log(error);
      }
  );
}

public closeAccordion(){
  this.accordion.closeAll();

}
resetForm(form:  NgForm)  {         
  form.resetForm();  
}

refreshPage() {
  this._document.defaultView.location.reload();
}
 clearForm(form:  NgForm) {
 // var lista = JSON.parse(JSON.stringify(this.list));
 
}




}
