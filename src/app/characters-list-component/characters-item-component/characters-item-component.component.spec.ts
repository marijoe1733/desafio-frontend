import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharactersItemComponentComponent } from './characters-item-component.component';

describe('CharactersItemComponentComponent', () => {
  let component: CharactersItemComponentComponent;
  let fixture: ComponentFixture<CharactersItemComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharactersItemComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersItemComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
