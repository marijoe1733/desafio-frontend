
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  data ={};

  getcharacters() {
    return this.http.get('http://localhost:8000/api/characters').pipe(
      tap((respuesta: any) => {
        return respuesta;
        
        
      })
    );
  }

  updatecharacter(data) {

    var nameCharacter = data.name;
    var locationCharacter = data.location
   

    return this.http.put('http://localhost:8000/api/character/'+`${data.id}`,{nameCharacter, locationCharacter}).pipe(
      tap((respuesta: any) => {
        return respuesta;
        
        
      })
    );
  }
}
