import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EpisodesService {

  constructor(private http: HttpClient) { }


  getEpisodes() {
    return this.http.get('http://localhost:8000/api/episodes').pipe(
      tap((respuesta: any) => {
       // console.log(respuesta)
        return respuesta;
        
        
      })
    );
  }
}
