import { Component, OnInit } from '@angular/core';
import { EpisodesService} from "../episodes.service";


@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent implements OnInit {

  list = [];
  title = "Episodes";
  constructor(public _listado: EpisodesService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this._listado.getEpisodes().subscribe(
        (res: any) => {
           
            this.list = res;
            console.log(this.list);
        },
        (error) => {
            console.log(error);
        }
    );
}

}
